import spacecan


def sync_received():
    print("sync")


def telecommand_received(self, can_frame):
    source = spacecan.Packet.from_can_frame(can_frame)

    # send command acceptance report
    reply = spacecan.Packet(service=1, subtype=1, data=[source.service, source.subtype])
    self.parent.send_telemetry(reply)

    # execute command
    print("ping")
    reply = spacecan.Packet(service=17, subtype=2)
    self.parent.send_telemetry(reply)

    # send command execution report
    reply = spacecan.Packet(service=1, subtype=7, data=[source.service, source.subtype])
    self.parent.send_telemetry(reply)


responder = spacecan.Responder(node_id=2, hb_period=0.5, max_hb_miss=5)
responder.sync.handler = sync_received
responder.telecommand.handler = telecommand_received
responder.connect(interface="socketcan", channel_a="vcan0", channel_b="vcan1")
responder.start()

try:
    while True:
        pass
except KeyboardInterrupt:
    pass

responder.stop()
responder.disconnect()
