"""This is an example of a controller node.

The controller node connects to both virtual CAN busses.

It continously:
- sends heartbeats on the selected active bus
- sends sync on the active bus

"""
import time
import spacecan


def telemetry_received(self, can_frame):
    source = spacecan.Packet.from_can_frame(can_frame)
    if source.service == 1 and source.subtype == 1:
        print(f"\nAcceptance report for ({source.data[0]}, {source.data[1]})")
    elif source.service == 1 and source.subtype == 7:
        print(f"Execution success report for ({source.data[0]}, {source.data[1]})")
    else:
        print("Received TM:", can_frame)


controller = spacecan.Controller(hb_period=0.5, sync_period=5)
controller.telemetry.handler = telemetry_received
controller.connect(interface="socketcan", channel_a="vcan0", channel_b="vcan1")
controller.start()

while True:
    x = input("(q)uit; (b)us-switch; ping (1) or (2), (s)cet, (u)tc: ").lower()

    if x == "q":
        break

    elif x == "b":
        controller.switch_bus()

    elif x == "1":
        destination_node_id = 1
        packet = spacecan.Packet(service=17, subtype=1)
        print("Sending (17,1) to node 1")
        controller.send_telecommand(destination_node_id, packet)

    elif x == "2":
        destination_node_id = 2
        packet = spacecan.Packet(service=17, subtype=1)
        print("Sending (17,1) to node 2")
        controller.send_telecommand(destination_node_id, packet)

    elif x == "s":
        coarse_time = int(time.monotonic())
        fine_time = 0
        controller.send_scet(coarse_time, fine_time)

    elif x == "u":
        day = int(time.time() / 60 / 60 / 24)  # days since 1970-01-01
        ms_of_day = 0
        sub_ms_of_day = 0
        controller.send_utc(day, ms_of_day, sub_ms_of_day)

controller.stop()
controller.disconnect()
