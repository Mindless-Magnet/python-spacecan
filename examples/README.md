# Example

In this example, a controller is created that has two responders attached to it.

It is using the socketcan interface, available on Linux. To make it very easy,
we are creating a virtual CAN interface, which does not need any actual physical
CAN bus hardware.

## Create virtual CAN interface

To create the virtual CAN interface, run the following:

- Load vcan kernel module: `sudo modprobe vcan`
- Create the virtual CAN interface: `sudo ip link add dev vcan0 type vcan`
- Bring the virtual CAN interface online: `sudo ip link set up vcan0`

Do the same for creating the redundant bus `vcan1`.

## Install can-utils for Linux (optional)

If you want to monitor the traffic on the virtual can buses, install `can-utils`
of your Linux distribution. For Ubuntu/Debian: `sudo apt install can-utils`.

To monitor the traffic on vcan0: `candump -tz vcan0`.

## Run the Controller and Responder Nodes

Load the virtual environment and start the controller:

```
$ . venv/bin/activate
(venv)$ cd examples
(venv)$ python controller.py
```

Do the same for the two responders.
