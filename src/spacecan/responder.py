from .network import Network
from .heartbeat import HeartbeatConsumer
from .sync import SyncConsumer
from .can_frame import (
    CanFrame,
    CanFrameHandler,
    FULL_MASK,
    ID_SYNC,
    ID_HEARTBEAT,
    ID_TC,
    ID_TM,
    ID_SCET,
    ID_UTC,
)


class Responder:
    def __init__(self, node_id, hb_period=None, max_hb_miss=3, max_bus_switch=None):
        self.node_id = node_id
        self.hb_period = hb_period
        self.max_hb_miss = max_hb_miss
        self.max_bus_switch = max_bus_switch

        self.heartbeat = HeartbeatConsumer(self) if hb_period else None
        self.sync = SyncConsumer(self)
        self.telecommand = CanFrameHandler(self)

    def connect(self, interface, channel_a, channel_b):
        if interface == "socketcan":
            from .bus.socketcan import SocketCanBus

            bus_a = SocketCanBus(channel=channel_a)
            bus_b = SocketCanBus(channel=channel_b)
            # receive sync, heartbeat, and telecommands from controller node
            filters = [
                {"can_id": ID_HEARTBEAT, "can_mask": FULL_MASK},
                {"can_id": ID_SYNC, "can_mask": FULL_MASK},
                {"can_id": ID_SCET, "can_mask": FULL_MASK},
                {"can_id": ID_UTC, "can_mask": FULL_MASK},
                {"can_id": ID_TC + self.node_id, "can_mask": FULL_MASK},
            ]
            bus_a.set_filters(filters)
            bus_b.set_filters(filters)
            self.network = Network(self, self.node_id, bus_a, bus_b)
        else:
            raise NotImplementedError

    def disconnect(self):
        self.network.bus_a.disconnect()
        self.network.bus_b.disconnect()

    def start(self):
        self.network.start()
        if self.heartbeat:
            self.heartbeat.start(self.hb_period, self.max_hb_miss, self.max_bus_switch)
        self.sync.start()
        self.telecommand.start()

    def stop(self):
        self.telecommand.stop()
        self.sync.stop()
        if self.heartbeat:
            self.heartbeat.stop()
        self.network.stop()

    def switch_bus(self):
        self.network.stop()
        if self.network.selected_bus == self.network.bus_a:
            self.network.selected_bus = self.network.bus_b
        elif self.network.selected_bus == self.network.bus_b:
            self.network.selected_bus = self.network.bus_a
        self.network.start()

    def send_telemetry(self, packet):
        can_id = ID_TM + self.node_id
        data = packet.encode()
        can_frame = CanFrame(can_id, data)
        self.network.send(can_frame)
