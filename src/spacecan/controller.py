from .network import Network
from .heartbeat import HeartbeatProducer
from .sync import SyncProducer
from .can_frame import (
    CanFrame,
    CanFrameHandler,
    FUNCTION_MASK,
    ID_TM,
    ID_TC,
    ID_SCET,
    ID_UTC,
    ID_SYNC,
)


class Controller:
    def __init__(self, hb_period=None, sync_period=None):
        self.node_id = 0  # bus controller node id is always 0
        self.hb_period = hb_period
        self.sync_period = sync_period

        self.heartbeat = HeartbeatProducer(self) if hb_period else None
        self.sync = SyncProducer(self) if self.sync_period else None
        self.telemetry = CanFrameHandler(self)

    def connect(self, interface, channel_a, channel_b):
        if interface == "socketcan":
            from .bus.socketcan import SocketCanBus

            bus_a = SocketCanBus(channel=channel_a)
            bus_b = SocketCanBus(channel=channel_b)
            # receive telemetry from all responder nodes
            filters = [{"can_id": ID_TM, "can_mask": FUNCTION_MASK}]
            bus_a.set_filters(filters)
            bus_b.set_filters(filters)
            self.network = Network(self, self.node_id, bus_a, bus_b)
        else:
            raise NotImplementedError

    def disconnect(self):
        self.network.bus_a.disconnect()
        self.network.bus_b.disconnect()

    def start(self):
        self.network.start()
        if self.heartbeat:
            self.heartbeat.start(self.hb_period)
        if self.sync:
            self.sync.start(self.sync_period)
        self.telemetry.start()

    def stop(self):
        self.telemetry.stop()
        if self.sync:
            self.sync.stop()
        if self.heartbeat:
            self.heartbeat.stop()
        self.network.stop()

    def switch_bus(self):
        self.network.stop()
        if self.network.selected_bus == self.network.bus_a:
            self.network.selected_bus = self.network.bus_b
        elif self.network.selected_bus == self.network.bus_b:
            self.network.selected_bus = self.network.bus_a
        self.network.start()

    def send_telecommand(self, destination_node_id, packet):
        can_id = ID_TC + destination_node_id
        data = packet.encode()
        can_frame = CanFrame(can_id, data)
        self.network.send(can_frame)

    def send_scet(self, coarse_time, fine_time=0, precision=8):
        can_id = ID_SCET
        if precision == 8:
            fine_time = fine_time << 16
        elif precision == 16:
            fine_time = fine_time << 8
        data = bytearray(
            [
                fine_time >> 16,
                (fine_time >> 8) & 0xFF,
                fine_time & 0xFF,
                coarse_time >> 24,
                (coarse_time >> 16) & 0xFF,
                (coarse_time >> 8) & 0xFF,
                coarse_time & 0xFF,
            ]
        )
        can_frame = CanFrame(can_id, data)
        self.network.send(can_frame)

    def send_utc(self, day, ms_of_day, sub_ms=0):
        can_id = ID_UTC
        data = bytearray(
            [
                sub_ms >> 8,
                sub_ms & 0xFF,
                ms_of_day >> 24,
                (ms_of_day >> 16) & 0xFF,
                (ms_of_day >> 8) & 0xFF,
                ms_of_day & 0xFF,
                day >> 8,
                day & 0xFF,
            ]
        )
        can_frame = CanFrame(can_id, data)
        self.network.send(can_frame)

    def send_sync(self):
        can_id = ID_SYNC
        can_frame = CanFrame(can_id, bytearray())
        self.network.send(can_frame)
