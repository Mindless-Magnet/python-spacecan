class Packet:
    def __init__(self, service, subtype, n=0, data=None):
        self.service = service
        self.subtype = subtype
        self.n = n
        self.data = data

    def __repr__(self):
        return f"Packet({self.service}, {self.subtype}, {self.n}, {list(self.data)})"

    def encode(self):
        databytes = []
        databytes.append(self.service)
        databytes.append(self.subtype)
        databytes.append(self.n)
        if self.data is not None:
            databytes.extend(self.data)
        return databytes

    @classmethod
    def from_can_frame(cls, can_frame):
        service = can_frame.data[0]
        subtype = can_frame.data[1]
        n = can_frame.data[2]
        data = can_frame.data[3:]
        return cls(service, subtype, n, data)
