import threading
import queue

from .can_frame import FUNCTION_MASK, NODE_MASK, ID_HEARTBEAT, ID_SYNC, ID_TC, ID_TM


class Network:
    """
    The Network class represents the redundant CAN system bus. It is
    initialized with a node ID and two bus objects, of which the nominal
    bus will be the selected bus (until the bus is switched).

    """

    def __init__(self, parent, node_id, bus_a, bus_b):
        self.parent = parent
        self.node_id = node_id
        self.bus_a = bus_a
        self.bus_b = bus_b

        self.selected_bus = self.bus_a
        self.sync_handler = None

        self._thread = None

    def start(self):
        self.selected_bus.flush_frame_buffer()
        self.selected_bus.start_receive()
        self._thread = threading.Thread(target=self._process)
        self._thread.kill = False
        self._thread.start()

    def stop(self):
        self.selected_bus.flush_frame_buffer()
        self.selected_bus.stop_receive()
        if self._thread:
            self._thread.kill = True
            self._thread.join()

    def _process(self):
        thread = threading.current_thread()

        while not thread.kill:
            try:
                can_frame = self.selected_bus.frame_buffer.get(timeout=0.1)
            except queue.Empty:
                can_frame = None
                continue
            if thread.kill:
                break

            func_id = can_frame.can_id & FUNCTION_MASK
            node_id = can_frame.can_id & NODE_MASK

            if func_id == ID_HEARTBEAT:
                if self.parent.heartbeat:
                    self.parent.heartbeat.received()

            elif func_id == ID_SYNC:
                if self.parent.sync:
                    self.parent.sync.received()

            # responder node receives TC
            elif func_id == ID_TC:
                if node_id == self.node_id:
                    if self.parent.telecommand:
                        self.parent.telecommand.received(can_frame)

            # controller node receives TM
            elif func_id == ID_TM:
                if self.parent.telemetry:
                    self.parent.telemetry.received(can_frame)

    def send(self, can_frame):
        self.selected_bus.send(can_frame)
