import spacecan


def test_controller():
    controller = spacecan.Controller(hb_period=0.5, sync_period=5)
    assert controller


def test_responder():
    responder = spacecan.Responder(node_id=1, hb_period=0.5)
    assert responder
